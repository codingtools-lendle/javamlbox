/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rocks.imsofa.wekatest;

import weka.core.AttributeStats;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;;

/**
 *
 * @author lendle
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception{
        // TODO code application logic here
        DataSource source = new DataSource("/home/lendle/programs/weka-3-8-1/data/iris.arff");
        Instances data = source.getDataSet();
        AttributeStats as=data.attributeStats(0);
        System.out.println(as.numericStats.stdDev);
        System.out.println(as.numericStats.mean);
    }
    
}
