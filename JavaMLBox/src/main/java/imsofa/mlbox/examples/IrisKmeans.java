/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.examples;

import imsofa.mlbox.datamodel.DataFrame;
import imsofa.mlbox.runtime.Core;
import imsofa.mlbox.runtime.CoreFactory;
import imsofa.mlbox.runtime.DataFrameLoader;
import imsofa.mlbox.runtime.DataFrameWriter;
import imsofa.mlbox.runtime.clustering.Clustering;
import imsofa.mlbox.runtime.clustering.KmeansParameter;
import imsofa.mlbox.runtime.clustering.KmeansResult;
import java.io.File;

/**
 *
 * @author lendle
 */
public class IrisKmeans {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception{
        // TODO code application logic here
        Core core=CoreFactory.getCore();
        DataFrameLoader loader=core.getDataFrameLoader();
        DataFrameWriter dataFrameWriter=core.getDataFrameWriter();
        Clustering clustering=core.getClustering();
        
        DataFrame df=loader.load(new File("iris.csv"), "utf-8");
        DataFrame features=df.project(new String[]{"sepallength", "sepalwidth", "petallength", "petalwidth"});
        
        KmeansParameter kmeansParameter=new KmeansParameter(features, 3);
        KmeansResult kmeansResult=clustering.getKmeans().calculate(kmeansParameter);
        df.addColumn("cluster", Double.class, kmeansResult.getClusters());
        
        System.out.println(df);
    }
    
}
