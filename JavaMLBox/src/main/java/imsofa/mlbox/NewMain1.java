/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox;

import imsofa.mlbox.datamodel.DataFrame;
import imsofa.mlbox.runtime.DataFrameLoader;
import imsofa.mlbox.runtime.clustering.Kmeans;
import imsofa.mlbox.runtime.clustering.KmeansParameter;
import imsofa.mlbox.runtime.clustering.KmeansResult;
import imsofa.mlbox.runtime.impl.clustering.RKmeansImpl;
import imsofa.mlbox.runtime.impl.weka.WekaDataFrameLoaderImpl;
import java.io.File;
import java.util.Arrays;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import org.renjin.sexp.IntArrayVector;
import org.renjin.sexp.ListVector;

/**
 *
 * @author lendle
 */
public class NewMain1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        ScriptEngine scriptEngine=new ScriptEngineManager().getEngineByName("Renjin");
        scriptEngine.eval("name <- c(\"Jon\", \"Bill\", \"Maria\", \"Maria\", \"Maria\", \"Maria\", \"Maria\", \"Maria\", \"Maria\")");
        scriptEngine.eval("age <- c(23, 41, 32,23, 41, 32,23, 41, 32)");
        scriptEngine.eval("df=data.frame(name, age)");
        scriptEngine.eval("df$name=as.character(df$name)");
        scriptEngine.put("abc", new String[]{"a", "b", "c","a", "b", "c","a", "b", "c"});
        scriptEngine.eval("df$abc=as.character(abc)");
        scriptEngine.put("def", new double[]{3,4,5,3,4,5,3,4,5});
        scriptEngine.eval("df$def=def");
        ListVector ret=(ListVector) scriptEngine.eval("df");
        ListVector kmeans=(ListVector) scriptEngine.eval("kmeans(df[,c(2,4)], centers=3)");
        System.out.println(ret);
        System.out.println(kmeans);
        System.out.println(((org.renjin.primitives.R$primitive$$minus$deferred_dd)kmeans.get("betweenss")).asReal());
        System.out.println(((IntArrayVector)kmeans.get("cluster")));
        System.out.println(((org.renjin.primitives.summary.DeferredSum)kmeans.get("totss")).asReal());
        System.out.println(((org.renjin.sexp.DoubleArrayVector)kmeans.get("centers")).toDoubleArray());
        
        DataFrameLoader loader=new WekaDataFrameLoaderImpl();
        DataFrame df=loader.load(new File("iris.csv"), "utf-8");
        Kmeans kmeans1=new RKmeansImpl();
        KmeansParameter parameter=new KmeansParameter(df.project(new String[]{"sepallength", "petallength"}), 3);
        KmeansResult kmeansResult=kmeans1.calculate(parameter);
        System.out.println(kmeansResult.getInterClusterDistances()/kmeansResult.getTotalDistances());
        System.out.println(Arrays.toString(kmeansResult.getClusters().toDoubleArray()));
    }
    
}
