/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox;

import imsofa.mlbox.datamodel.DataFrame;
import imsofa.mlbox.datamodel.impl.weka.WekaDataFrameImpl;
import imsofa.mlbox.runtime.Core;
import imsofa.mlbox.runtime.CoreFactory;
import imsofa.mlbox.runtime.DataFrameLoader;
import imsofa.mlbox.runtime.DataFrameWriter;
import imsofa.mlbox.runtime.Stats;
import imsofa.mlbox.runtime.Tests;
import imsofa.mlbox.runtime.merger.AggregatingAttribute;
import imsofa.mlbox.runtime.merger.AggregatingMethod;
import imsofa.mlbox.runtime.merger.Aggregator;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import weka.core.Attribute;
import weka.core.Instances;

/**
 *
 * @author lendle
 */
public class NewMain2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception{
        // TODO code application logic here
        Core core=CoreFactory.getCore();
        DataFrameLoader loader=core.getDataFrameLoader();
        DataFrameWriter dataFrameWriter=core.getDataFrameWriter();
        DataFrame df=loader.load(new File("iris.csv"), "utf-8");
        Stats stats=core.getStats();
        Tests tests=core.getTests();
        Aggregator aggregator=core.getAggregator();
        
        System.out.println(stats.mean(df.getDataList("sepallength")));
        System.out.println(stats.sd(df.getDataList("sepallength")));
        System.out.println(stats.min(df.getDataList("sepallength")));
        System.out.println(stats.max(df.getDataList("sepallength")));
        System.out.println(Arrays.toString(stats.quantile(df.getDataList("sepallength"))));
        System.out.println(tests.kendallsCorr(df.getDataList("sepallength"), df.getDataList("petallength")));
        System.out.println(tests.pearsonCorr(df.getDataList("sepallength"), df.getDataList("petallength")));
        System.out.println(tests.spearmanCorr(df.getDataList("sepallength"), df.getDataList("petallength")));
        System.out.println(tests.tTest(df.getDataList("sepallength"), df.getDataList("petallength")));
        System.out.println(tests.pairedTTest(df.getDataList("sepallength"), df.getDataList("petallength")));
        DataFrame df2=df.project(new String[]{"petallength", "petalwidth"});
        System.out.println(df2.getAttributes().get(0).getName()+":"+df.getAttributes().get(0).getName());
        DataFrame df4=df.filter(0, 4);
        df4.addColumn("t1", String.class, new Object[]{"A", "B", "C", "D", "E"});
        df4.addRow(new Object[]{1,2,3,4,"Setosa", "F"});
        System.out.println(df4);
        
        DataFrame df5=aggregator.aggregate(df, new String[]{"variety"}, new AggregatingAttribute[]{new AggregatingAttribute("sepallength", AggregatingMethod.mean),
            new AggregatingAttribute("sepalwidth", AggregatingMethod.mean)});
        System.out.println(df5);
        DataFrame df6=df.sort(new DataFrame.SortSpec[]{
            new DataFrame.SortSpec("variety"),
            new DataFrame.SortSpec("sepallength", false)
        });

        
        dataFrameWriter.save(df4, new File("testSave.csv"), "utf-8");
        DataFrame df7=loader.load(new File("testSave.csv"), "utf-8");
        System.out.println(df7);
        System.out.println(Arrays.deepToString(df7.getDataList("t1").toArray()));
    }
    
}
