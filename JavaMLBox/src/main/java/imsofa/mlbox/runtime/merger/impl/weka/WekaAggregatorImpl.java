/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime.merger.impl.weka;

import imsofa.mlbox.datamodel.DataAttribute;
import imsofa.mlbox.datamodel.DataFrame;
import imsofa.mlbox.datamodel.impl.weka.WekaDataFrameImpl;
import imsofa.mlbox.runtime.DataFrameFactory;
import imsofa.mlbox.runtime.impl.DefaultStatsImpl;
import imsofa.mlbox.runtime.impl.weka.WekaDataFrameFactoryImpl;
import imsofa.mlbox.runtime.merger.AggregatingAttribute;
import imsofa.mlbox.runtime.merger.Aggregator;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import weka.core.Instance;

/**
 *
 * @author lendle
 */
public class WekaAggregatorImpl implements Aggregator{
    @Override
    public DataFrame aggregate(DataFrame dataFrame, String[] groupingAttributes, AggregatingAttribute[] aggregatingAttributes) {
        Map<Index, List<Instance>> indexMap=Utils.convert2IndexMap((WekaDataFrameImpl) dataFrame, groupingAttributes);
        DataFrameFactory factory=new WekaDataFrameFactoryImpl();
        List<DataFrameFactory.AttributeSpec> attributeSpecs=new ArrayList<>();
        for(String name : groupingAttributes){
            DataAttribute attribute=dataFrame.getAttribute(name);
            attributeSpecs.add(new DataFrameFactory.AttributeSpec(name, attribute.getType().equals(String.class)));
        }
        for(AggregatingAttribute aggregatingAttribute : aggregatingAttributes){
            attributeSpecs.add(new DataFrameFactory.AttributeSpec(aggregatingAttribute.getAttributeName(), false));
        }
        DataFrame newDataFrame=factory.createDataFrame(attributeSpecs.toArray(new DataFrameFactory.AttributeSpec[0]));
        for(Index index : indexMap.keySet()){
            DataFrame subDataFrame=dataFrame.filter(new IndexDataFrameFilter(groupingAttributes, index));
            List row=new ArrayList();
            for(Object value : index){
                row.add(value);
            }
            for(int i=0; i<aggregatingAttributes.length; i++){
                double value=aggregatingAttributes[i].getMethod().calculate(subDataFrame, subDataFrame.getDataList(aggregatingAttributes[i].getAttributeName()));
                row.add(value);
            }
            newDataFrame.addRow(row.toArray());
        }
        return newDataFrame;
    }
}
