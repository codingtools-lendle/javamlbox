/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime.merger.impl;

import imsofa.mlbox.datamodel.DataFrame;
import imsofa.mlbox.datamodel.DataList;
import imsofa.mlbox.runtime.impl.DefaultStatsImpl;
import imsofa.mlbox.runtime.merger.AggregatingMethod;

/**
 *
 * @author lendle
 */
public class MedAggregatingMethod implements AggregatingMethod{

    @Override
    public double calculate(DataFrame dataFrame, DataList dataList) {
        if(Number.class.isAssignableFrom(dataList.getType())==false){
            throw new RuntimeException("only numeric values are allowed");
        }
        return new DefaultStatsImpl().median(dataList);
    }
    
}
