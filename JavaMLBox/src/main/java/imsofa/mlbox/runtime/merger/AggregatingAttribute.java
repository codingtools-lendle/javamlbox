/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime.merger;

/**
 *
 * @author lendle
 */
public class AggregatingAttribute {
    private String attributeName=null;
    private AggregatingMethod method=null;

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public AggregatingMethod getMethod() {
        return method;
    }

    public void setMethod(AggregatingMethod method) {
        this.method = method;
    }

    public AggregatingAttribute(String attributeName, AggregatingMethod method) {
        this.attributeName=attributeName;
        this.method=method;
    }
}
