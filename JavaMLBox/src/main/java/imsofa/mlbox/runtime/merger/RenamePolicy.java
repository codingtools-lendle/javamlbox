/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime.merger;

import imsofa.mlbox.datamodel.DataAttribute;
import imsofa.mlbox.datamodel.DataFrame;

/**
 * used in join when both DataFrames contains attributes with the same name
 */
public interface RenamePolicy {

    public String newName(DataFrame left, DataFrame right, DataFrame source, DataAttribute originalAttribute);

    public String newName(boolean left, DataAttribute originalAttribute);
}
