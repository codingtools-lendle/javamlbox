/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime.merger;

import imsofa.mlbox.datamodel.DataAttribute;

/**
 *
 * @author lendle
 */
public class MergingAttribute {
    private String leftAttribute=null;
    private String rightAttribute=null;

    public MergingAttribute(String leftAttribute, String rightAttribute) {
        this.leftAttribute=leftAttribute;
        this.rightAttribute=rightAttribute;
    }

    /**
     * assume only one attribute is specified
     * either for only a single data frame or
     * both data frames using the same attribute
     * @param leftAttribute 
     */
    public MergingAttribute(String leftAttribute) {
        this.leftAttribute=leftAttribute;
        this.rightAttribute=leftAttribute;
    }
    
    public String getLeftAttribute() {
        return leftAttribute;
    }

    public void setLeftAttribute(String leftAttribute) {
        this.leftAttribute = leftAttribute;
    }

    public String getRightAttribute() {
        return rightAttribute;
    }

    public void setRightAttribute(String rightAttribute) {
        this.rightAttribute = rightAttribute;
    }
    
    
}
