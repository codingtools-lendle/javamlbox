/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime.merger.impl.weka;

import imsofa.mlbox.datamodel.impl.weka.WekaDataAttributeImpl;
import java.util.ArrayList;
import weka.core.Instance;

/**
 * an Index is a list of values
 * @author lendle
 */
public class Index extends ArrayList{
    public Index(Instance data, WekaDataAttributeImpl [] attributes){
        for(WekaDataAttributeImpl attribute : attributes){
            if(attribute.getType().equals(String.class)){
                super.add(data.stringValue(attribute.getAttribute()));
            }else{
                super.add(data.value(attribute.getAttribute()));
            }
        }
    }

    public Index() {
    }
    
    
}
