/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime.merger;

import imsofa.mlbox.datamodel.DataFrame;

/**
 *
 * @author lendle
 */
public interface Aggregator {
    public DataFrame aggregate(DataFrame dataFrame, String [] groupingAttributes, AggregatingAttribute [] aggregatingAttributes);
}
