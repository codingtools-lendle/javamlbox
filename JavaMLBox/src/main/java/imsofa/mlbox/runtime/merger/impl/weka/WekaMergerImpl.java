/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime.merger.impl.weka;

import imsofa.mlbox.datamodel.DataAttribute;
import imsofa.mlbox.datamodel.DataFrame;
import imsofa.mlbox.datamodel.impl.weka.WekaDataAttributeImpl;
import imsofa.mlbox.datamodel.impl.weka.WekaDataFrameImpl;
import imsofa.mlbox.runtime.DataFrameLoader;
import imsofa.mlbox.runtime.merger.Merger;
import imsofa.mlbox.runtime.merger.impl.DefaultRenamePolicy;
import imsofa.mlbox.runtime.impl.weka.WekaDataFrameLoaderImpl;
import imsofa.mlbox.runtime.merger.MergingAttribute;
import imsofa.mlbox.runtime.merger.RenamePolicy;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

/**
 *
 * @author lendle
 */
public class WekaMergerImpl implements Merger {

    @Override
    public DataFrame join(DataFrame left, DataFrame right, MergingAttribute[] mergingAttributes) {
        return this.join(left, right, mergingAttributes, new DefaultRenamePolicy());
    }

    @Override
    public DataFrame leftJoin(DataFrame left, DataFrame right, MergingAttribute[] mergingAttributes) {
        return this.leftJoin(left, right, mergingAttributes, new DefaultRenamePolicy());
    }

    @Override
    public DataFrame rightJoin(DataFrame left, DataFrame right, MergingAttribute[] mergingAttributes) {
        return this.rightJoin(left, right, mergingAttributes, new DefaultRenamePolicy());
    }

    @Override
    public DataFrame join(DataFrame left, DataFrame right, MergingAttribute[] mergingAttributes, RenamePolicy renamePolicy) {
        return this.allJoinImplementations(left, right, JoinType.INNER, mergingAttributes, renamePolicy);
    }

    private DataFrame allJoinImplementations(DataFrame left, DataFrame right, JoinType joinType,
            MergingAttribute[] mergingAttributes, RenamePolicy renamePolicy) {
        WekaDataFrameImpl wekaLeft = (WekaDataFrameImpl) left;
        WekaDataFrameImpl wekaRight = (WekaDataFrameImpl) right;
        Instances leftInstances = wekaLeft.getInstances();
        Instances rightInstances = wekaRight.getInstances();
        WekaDataAttributeImpl[] leftKeyAttributes = new WekaDataAttributeImpl[mergingAttributes.length];
        WekaDataAttributeImpl[] rightKeyAttributes = new WekaDataAttributeImpl[mergingAttributes.length];
        for (int i = 0; i < mergingAttributes.length; i++) {
            leftKeyAttributes[i] = (WekaDataAttributeImpl) wekaLeft.getAttribute(mergingAttributes[i].getLeftAttribute());
            rightKeyAttributes[i] = (WekaDataAttributeImpl) wekaRight.getAttribute(mergingAttributes[i].getRightAttribute());
        }
        Map<Index, List<Instance>> leftIndexMap = Utils.convert2IndexMap(wekaLeft, leftKeyAttributes);
        Map<Index, List<Instance>> rightIndexMap = Utils.convert2IndexMap(wekaRight, rightKeyAttributes);
        List<AttributeSource> newAttributes = this.collectAttributeSources(leftKeyAttributes, rightKeyAttributes,
                left.getAttributes(), right.getAttributes(), renamePolicy);
        ArrayList<Attribute> attributeInfo = new ArrayList<>();
        for (AttributeSource attributeSource : newAttributes) {
            attributeInfo.add(attributeSource.getNewWekaAttribute());
        }
        Instances newInstances = new Instances("" + System.currentTimeMillis(), attributeInfo, 0);
        HashSet<Index> indexSet = new HashSet<>(leftIndexMap.keySet());
        for (Index index : indexSet) {
            List<Instance> leftData = leftIndexMap.remove(index);
            List<Instance> rightData = rightIndexMap.remove(index);
            //assume they both exists
            for (int i = 0; i < leftData.size(); i++) {
                Instance leftInstance = leftData.get(i);
                //we take index from left data, so left data will never be empty
                if (rightData == null && (joinType.equals(JoinType.INNER) || joinType.equals(JoinType.RIGHT))) {
                    continue;
                }
                if (rightData == null) {
                    DenseInstance instance = new DenseInstance(newAttributes.size());
                    for (AttributeSource attributeSource : newAttributes) {
                        if (attributeSource.isLeft()) {
                            if (attributeSource.getNewWekaAttribute().isString()) {
                                instance.setValue(attributeSource.getNewWekaAttribute(), leftInstance.stringValue(attributeSource.getOldWekaAttribute()));
                            } else {
                                instance.setValue(attributeSource.getNewWekaAttribute(), leftInstance.value(attributeSource.getOldWekaAttribute()));
                            }
                        } else {
                            instance.setMissing(attributeSource.getNewWekaAttribute());
                        }
                    }
                    newInstances.add(instance);
                } else {
                    for (int j = 0; j < rightData.size(); j++) {
                        Instance rightInstance = rightData.get(j);
                        //create a new Instance object
                        DenseInstance instance = new DenseInstance(newAttributes.size());
                        for (AttributeSource attributeSource : newAttributes) {
                            if (attributeSource.isLeft()) {
                                if (attributeSource.getNewWekaAttribute().isString()) {
                                    instance.setValue(attributeSource.getNewWekaAttribute(), leftInstance.stringValue(attributeSource.getOldWekaAttribute()));
                                } else {
                                    instance.setValue(attributeSource.getNewWekaAttribute(), leftInstance.value(attributeSource.getOldWekaAttribute()));
                                }
                            } else {
                                if (attributeSource.getNewWekaAttribute().isString()) {
                                    instance.setValue(attributeSource.getNewWekaAttribute(), rightInstance.stringValue(attributeSource.getOldWekaAttribute()));
                                } else {
                                    instance.setValue(attributeSource.getNewWekaAttribute(), rightInstance.value(attributeSource.getOldWekaAttribute()));
                                }
                            }
                        }
                        newInstances.add(instance);
                    }
                }
            }

        }
        if (!rightIndexMap.isEmpty()) {
            if (joinType.equals(JoinType.RIGHT)) {
                for (Index index : rightIndexMap.keySet()) {
                    List<Instance> rightData = rightIndexMap.get(index);
                    for (Instance rightInstance : rightData) {
                        DenseInstance instance = new DenseInstance(newAttributes.size());
                        for (AttributeSource attributeSource : newAttributes) {
                            if (!attributeSource.isLeft()) {
                                if (attributeSource.getNewWekaAttribute().isString()) {
                                    instance.setValue(attributeSource.getNewWekaAttribute(), rightInstance.stringValue(attributeSource.getOldWekaAttribute()));
                                } else {
                                    instance.setValue(attributeSource.getNewWekaAttribute(), rightInstance.value(attributeSource.getOldWekaAttribute()));
                                }
                            } else {
                                instance.setMissing(attributeSource.getNewWekaAttribute());
                            }
                        }
                        newInstances.add(instance);
                    }
                }
            }
        }
        return new WekaDataFrameImpl(newInstances);
    }

    private List<AttributeSource> collectAttributeSources(WekaDataAttributeImpl[] leftKeyAttributes,
            WekaDataAttributeImpl[] rightKeyAttributes, List<DataAttribute> leftAttributes, List<DataAttribute> rightAttributes,
            RenamePolicy renamePolicy) {
        Map<String, String> keyAttributeNameMap = new HashMap<>();
        Map<String, String> nonKeyLeftAttributeNameMap = new HashMap<>();
        Map<String, String> nonKeyRightAttributeNameMap = new HashMap<>();
        List<AttributeSource> ret = new ArrayList<>();
        for (WekaDataAttributeImpl attribute : leftKeyAttributes) {
            keyAttributeNameMap.put(attribute.getName(), "");
            //attribute in left key array will always be kept
            ret.add(new AttributeSource(attribute.getName(), attribute.getName(), attribute.getAttribute(),
                    (attribute.getType().equals(String.class)) ? new Attribute(attribute.getName(), true) : new Attribute(attribute.getName()), true));
        }
        for (WekaDataAttributeImpl attribute : rightKeyAttributes) {
            if (keyAttributeNameMap.containsKey(attribute.getName()) == false) {
                //attribute in right key array will be kept only if they have different names with their left counterpart
                keyAttributeNameMap.put(attribute.getName(), "");
                ret.add(new AttributeSource(attribute.getName(), attribute.getName(), attribute.getAttribute(),
                        (attribute.getType().equals(String.class)) ? new Attribute(attribute.getName(), true) : new Attribute(attribute.getName()), false));
            }
        }
        for (DataAttribute attribute : leftAttributes) {
            nonKeyLeftAttributeNameMap.put(attribute.getName(), "");
        }
        for (DataAttribute attribute : rightAttributes) {
            nonKeyRightAttributeNameMap.put(attribute.getName(), "");
        }
        for (DataAttribute attribute : leftAttributes) {
            if (keyAttributeNameMap.containsKey(attribute.getName()) == false) {
                //attribute in left df will be bept if they are not key
                //but if their names are conflicted with right df, they will be renamed
                String newName = renamePolicy.newName(true, attribute);
                if (nonKeyRightAttributeNameMap.containsKey(attribute.getName())) {
                    ret.add(new AttributeSource(attribute.getName(), newName, ((WekaDataAttributeImpl) attribute).getAttribute(),
                            (attribute.getType().equals(String.class)) ? new Attribute(newName, true) : new Attribute(newName), true));
                } else {
                    ret.add(new AttributeSource(attribute.getName(), attribute.getName(), ((WekaDataAttributeImpl) attribute).getAttribute(),
                            (attribute.getType().equals(String.class)) ? new Attribute(attribute.getName(), true) : new Attribute(attribute.getName()), true));
                }
            }
        }
        for (DataAttribute attribute : rightAttributes) {
            if (keyAttributeNameMap.containsKey(attribute.getName()) == false) {
                //attribute in right df will be bept if they are not key
                //but if their names are conflicted with left df, they will be renamed
                String newName = renamePolicy.newName(false, attribute);
                if (nonKeyLeftAttributeNameMap.containsKey(attribute.getName())) {
                    ret.add(new AttributeSource(attribute.getName(), renamePolicy.newName(false, attribute), ((WekaDataAttributeImpl) attribute).getAttribute(),
                            (attribute.getType().equals(String.class)) ? new Attribute(newName, true) : new Attribute(newName), false));
                } else {
                    ret.add(new AttributeSource(attribute.getName(), attribute.getName(), ((WekaDataAttributeImpl) attribute).getAttribute(),
                            (attribute.getType().equals(String.class)) ? new Attribute(attribute.getName(), true) : new Attribute(attribute.getName()), false));
                }
            }
        }
        return ret;
    }

    @Override
    public DataFrame leftJoin(DataFrame left, DataFrame right, MergingAttribute[] mergingAttributes, RenamePolicy renamePolicy) {
        return this.allJoinImplementations(left, right, JoinType.LEFT, mergingAttributes, renamePolicy);
    }

    @Override
    public DataFrame rightJoin(DataFrame left, DataFrame right, MergingAttribute[] mergingAttributes, RenamePolicy renamePolicy) {
        return this.allJoinImplementations(left, right, JoinType.RIGHT, mergingAttributes, renamePolicy);
    }

    class AttributeSource {

        private boolean left = true;
        private Attribute oldWekaAttribute = null;
        private String oldAttributeName = null;
        private String newAttributeName = null;
        private Attribute newWekaAttribute = null;

        public AttributeSource(String oldAttributeName, String newAttributeName, Attribute oldwekaAttribute, Attribute wekaAttribute, boolean left) {
            this.oldAttributeName = oldAttributeName;
            this.newAttributeName = newAttributeName;
            this.newWekaAttribute = wekaAttribute;
            this.oldWekaAttribute = oldwekaAttribute;
            this.left = left;
        }

        public Attribute getOldWekaAttribute() {
            return oldWekaAttribute;
        }

        public void setOldWekaAttribute(Attribute oldWekaAttribute) {
            this.oldWekaAttribute = oldWekaAttribute;
        }

        public Attribute getNewWekaAttribute() {
            return newWekaAttribute;
        }

        public void setNewWekaAttribute(Attribute newWekaAttribute) {
            this.newWekaAttribute = newWekaAttribute;
        }

        public String getNewAttributeName() {
            return newAttributeName;
        }

        public void setNewAttributeName(String newAttributeName) {
            this.newAttributeName = newAttributeName;
        }

        public boolean isLeft() {
            return left;
        }

        public void setLeft(boolean left) {
            this.left = left;
        }

        public String getOldAttributeName() {
            return oldAttributeName;
        }

        public void setOldAttributeName(String oldAttributeName) {
            this.oldAttributeName = oldAttributeName;
        }

    }

    enum JoinType {
        INNER,
        LEFT,
        RIGHT
    }

    public static void main(String[] args) throws Exception {
        DataFrameLoader loader = new WekaDataFrameLoaderImpl();
        DataFrame test = loader.load(new File("test.csv"), "utf-8");
        DataFrame test1 = loader.load(new File("test1.csv"), "utf-8");
        WekaMergerImpl merger = new WekaMergerImpl();
//        DataFrame ret = merger.join(test, test1, new MergingAttribute[]{
//            new MergingAttribute("col1"),
//            new MergingAttribute("col2")
//        });
//        DataFrame ret = merger.leftJoin(test, test1, new MergingAttribute[]{
//            new MergingAttribute("col1"),
//            new MergingAttribute("col2")
//        });
        DataFrame ret = merger.rightJoin(test, test1, new MergingAttribute[]{
            new MergingAttribute("col1"),
            new MergingAttribute("col2")
        });
        System.out.println(((WekaDataFrameImpl) ret).getInstances());
    }
}
