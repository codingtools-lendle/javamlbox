/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime.merger.impl.weka;

import imsofa.mlbox.datamodel.impl.weka.WekaDataAttributeImpl;
import imsofa.mlbox.datamodel.impl.weka.WekaDataFrameImpl;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import weka.core.Instance;

/**
 *
 * @author lendle
 */
public class Utils {
    protected static Map<Index, List<Instance>> convert2IndexMap(WekaDataFrameImpl df, WekaDataAttributeImpl[] keyAttributes) {
        Map<Index, List<Instance>> ret = new HashMap<>();
        for (int i = 0; i < df.getInstances().size(); i++) {
            Instance row = df.getInstances().get(i);
            Index index = new Index(row, keyAttributes);
            List<Instance> mapData = ret.get(index);
            if (mapData == null) {
                mapData = new ArrayList<>();
                ret.put(index, mapData);
            }
            mapData.add(row);
        }
        return ret;
    }
    
    protected static Map<Index, List<Instance>> convert2IndexMap(WekaDataFrameImpl df, String [] keyAttributes) {
        List<WekaDataAttributeImpl> keyWekaDataAttributes=new ArrayList<>();
        for(String keyAttribute : keyAttributes){
            keyWekaDataAttributes.add((WekaDataAttributeImpl) df.getAttribute(keyAttribute));
        }
        return convert2IndexMap(df, keyWekaDataAttributes.toArray(new WekaDataAttributeImpl[0]));
    }
}
