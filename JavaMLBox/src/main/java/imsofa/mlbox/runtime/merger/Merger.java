/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime.merger;

import imsofa.mlbox.datamodel.DataFrame;
import imsofa.mlbox.runtime.merger.MergingAttribute;
import imsofa.mlbox.runtime.merger.RenamePolicy;

/**
 *
 * @author lendle
 */
public interface Merger {
    public DataFrame join(DataFrame left, DataFrame right, MergingAttribute [] mergingAttributes);
    public DataFrame leftJoin(DataFrame left, DataFrame right, MergingAttribute [] mergingAttributes);
    public DataFrame rightJoin(DataFrame left, DataFrame right, MergingAttribute [] mergingAttributes);
    public DataFrame join(DataFrame left, DataFrame right, MergingAttribute [] mergingAttributes, RenamePolicy renamePolicy);
    public DataFrame leftJoin(DataFrame left, DataFrame right, MergingAttribute [] mergingAttributes, RenamePolicy renamePolicy);
    public DataFrame rightJoin(DataFrame left, DataFrame right, MergingAttribute [] mergingAttributes, RenamePolicy renamePolicy);
}
