/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime.merger.impl.weka;

import imsofa.mlbox.datamodel.DataFrame;
import imsofa.mlbox.datamodel.DataFrameFilter;
import imsofa.mlbox.datamodel.DataFrameRow;

/**
 *
 * @author lendle
 */
public class IndexDataFrameFilter implements DataFrameFilter{
    private String [] attributeNames=null;
    private Index index=null;

    public IndexDataFrameFilter(String [] attributeNames, Index index) {
        this.attributeNames=attributeNames;
        this.index=index;
    }
    
    @Override
    public boolean accept(DataFrame dataFrame, DataFrameRow row, int rowIndex) {
        Index newIndex=new Index();
        for(String attributeName : attributeNames){
            newIndex.add(row.getValue(attributeName));
        }
        if(index.equals(newIndex)){
            return true;
        }else{
            return false;
        }
    }
}
