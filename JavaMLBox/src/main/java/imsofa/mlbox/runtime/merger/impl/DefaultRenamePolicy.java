/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime.merger.impl;

import imsofa.mlbox.datamodel.DataAttribute;
import imsofa.mlbox.datamodel.DataFrame;
import imsofa.mlbox.runtime.merger.RenamePolicy;

/**
 *
 * @author lendle
 */
public class DefaultRenamePolicy implements RenamePolicy{

    @Override
    public String newName(DataFrame left, DataFrame right, DataFrame source, DataAttribute originalAttribute) {
        return newName(source==left, originalAttribute);
    }

    @Override
    public String newName(boolean left, DataAttribute originalAttribute) {
        if(left){
            return originalAttribute.getName()+".L";
        }else{
            return originalAttribute.getName()+".R";
        }
    }
    
}
