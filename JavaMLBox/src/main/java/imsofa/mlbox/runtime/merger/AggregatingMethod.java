/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime.merger;

import imsofa.mlbox.datamodel.DataFrame;
import imsofa.mlbox.datamodel.DataList;
import imsofa.mlbox.runtime.merger.impl.MeanAggregatingMethod;
import imsofa.mlbox.runtime.merger.impl.MedAggregatingMethod;
import imsofa.mlbox.runtime.merger.impl.SDAggregatingMethod;
import imsofa.mlbox.runtime.merger.impl.SumAggregatingMethod;
import imsofa.mlbox.runtime.merger.impl.VarAggregatingMethod;

/**
 *
 * @author lendle
 */
public interface AggregatingMethod {
    public double calculate(DataFrame dataFrame, DataList dataList);
    public static AggregatingMethod sum=new SumAggregatingMethod();
    public static AggregatingMethod mean=new MeanAggregatingMethod();
    public static AggregatingMethod med=new MedAggregatingMethod();
    public static AggregatingMethod sd=new SDAggregatingMethod();
    public static AggregatingMethod var=new VarAggregatingMethod();
}
