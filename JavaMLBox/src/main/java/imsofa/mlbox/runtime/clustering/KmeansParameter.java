/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime.clustering;

import imsofa.mlbox.datamodel.DataFrame;

/**
 *
 * @author lendle
 */
public class KmeansParameter {
    private int centers=-1;
    private DataFrame dataFrame=null;
    private int maxInterations=10;

    public KmeansParameter(DataFrame dataFrame, int centers, int maxIterations) {
        this.centers=centers;
        this.dataFrame=dataFrame;
        this.maxInterations=maxIterations;
    }

    public KmeansParameter(DataFrame dataFrame, int centers) {
        this(dataFrame, centers, 10);
    }

    public int getCenters() {
        return centers;
    }

    public void setCenters(int centers) {
        this.centers = centers;
    }

    public DataFrame getDataFrame() {
        return dataFrame;
    }

    public void setDataFrame(DataFrame dataFrame) {
        this.dataFrame = dataFrame;
    }

    public int getMaxInterations() {
        return maxInterations;
    }

    public void setMaxInterations(int maxInterations) {
        this.maxInterations = maxInterations;
    }
    
    
}
