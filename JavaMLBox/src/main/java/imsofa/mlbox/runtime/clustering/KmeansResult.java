/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime.clustering;

import imsofa.mlbox.datamodel.DataFrameRow;
import imsofa.mlbox.datamodel.DataList;

/**
 *
 * @author lendle
 */
public class KmeansResult {
    private DataList clusters=null;
    private DataFrameRow [] centers=null;
    private double totalDistances=-1;
    private double interClusterDistances=-1;

    public DataList getClusters() {
        return clusters;
    }

    public void setClusters(DataList clusters) {
        this.clusters = clusters;
    }

    public DataFrameRow[] getCenters() {
        return centers;
    }

    public void setCenters(DataFrameRow[] centers) {
        this.centers = centers;
    }

    public double getTotalDistances() {
        return totalDistances;
    }

    public void setTotalDistances(double totalDistances) {
        this.totalDistances = totalDistances;
    }

    public double getInterClusterDistances() {
        return interClusterDistances;
    }

    public void setInterClusterDistances(double interClusterDistances) {
        this.interClusterDistances = interClusterDistances;
    }
    
    
}
