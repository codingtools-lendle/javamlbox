/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime;

import imsofa.mlbox.datamodel.DataList;
import imsofa.mlbox.runtime.tests.TTestResult;

/**
 *
 * @author lendle
 */
public interface Tests {
    public double pearsonCorr(DataList list1, DataList list2);
    public double kendallsCorr(DataList list1, DataList list2);
    public double spearmanCorr(DataList list1, DataList list2);
    public TTestResult tTest(DataList list1, DataList list2) throws Exception;
    public TTestResult pairedTTest(DataList list1, DataList list2) throws Exception;
}
