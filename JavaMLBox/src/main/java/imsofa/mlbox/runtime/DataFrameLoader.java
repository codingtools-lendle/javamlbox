/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime;

import imsofa.mlbox.datamodel.DataFrame;
import java.io.File;
import java.io.Reader;

/**
 *
 * @author lendle
 */
public interface DataFrameLoader {
    public DataFrame load(File file, String charset) throws Exception;
}
