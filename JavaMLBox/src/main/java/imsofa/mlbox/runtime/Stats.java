/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime;

import imsofa.mlbox.datamodel.DataList;

/**
 *
 * @author lendle
 */
public interface Stats {
    public double sum(DataList list);
    public double mean(DataList list);
    public double median(DataList list);
    public double sd(DataList list);
    public double variance(DataList list);
    public double max(DataList list);
    public double min(DataList list);
    public double[] quantile(DataList list, double [] positions);
    /**
     * default to [0, 25, 50, 75, 100]
     * @param list
     * @return 
     */
    public double[] quantile(DataList list);
}
