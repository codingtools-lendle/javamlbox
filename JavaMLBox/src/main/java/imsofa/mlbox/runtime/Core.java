/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime;

import imsofa.mlbox.runtime.clustering.Clustering;
import imsofa.mlbox.runtime.merger.Aggregator;
import imsofa.mlbox.runtime.merger.Merger;

/**
 *
 * @author lendle
 */
public interface Core {
    public DataFrameLoader getDataFrameLoader();
    public DataFrameWriter getDataFrameWriter();
    public DataFrameFactory getDataFrameFactory();
    public Stats getStats();
    public Tests getTests();
    public Merger getMerger();
    public Aggregator getAggregator();
    public Clustering getClustering();
}
