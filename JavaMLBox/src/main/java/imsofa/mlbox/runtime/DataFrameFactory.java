/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime;

import imsofa.mlbox.datamodel.DataFrame;

/**
 *
 * @author lendle
 */
public interface DataFrameFactory {
    public DataFrame createDataFrame(AttributeSpec [] attributes);
    
    public class AttributeSpec{
        private String name;
        private boolean stringType=false;

        public AttributeSpec(String name, boolean stringType) {
            this.name = name;
            this.stringType=stringType;
        }
        
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public boolean isStringType() {
            return stringType;
        }

        public void setStringType(boolean stringType) {
            this.stringType = stringType;
        }
        
    }
}
