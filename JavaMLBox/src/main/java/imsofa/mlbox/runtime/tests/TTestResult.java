/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime.tests;

import java.util.Arrays;

/**
 *
 * @author lendle
 */
public class TTestResult {
    private double p=-1;
    private double t=-1;
    private double df=-1;
    private double [] confidenceInterval=null;

    public double getP() {
        return p;
    }

    public void setP(double p) {
        this.p = p;
    }

    public double getT() {
        return t;
    }

    public void setT(double t) {
        this.t = t;
    }

    public double getDf() {
        return df;
    }

    public void setDf(double df) {
        this.df = df;
    }

    public double[] getConfidenceInterval() {
        return confidenceInterval;
    }

    public void setConfidenceInterval(double[] confidenceInterval) {
        this.confidenceInterval = confidenceInterval;
    }

    @Override
    public String toString() {
        return "TTestResult{" + "p=" + p + ", t=" + t + ", df=" + df + ", confidenceInterval=" + Arrays.toString(confidenceInterval) + '}';
    }
    
    
}
