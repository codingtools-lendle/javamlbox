/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime.impl.weka;

import imsofa.mlbox.datamodel.DataFrame;
import imsofa.mlbox.datamodel.impl.weka.WekaDataFrameImpl;
import imsofa.mlbox.runtime.DataFrameFactory;
import java.util.ArrayList;
import weka.core.Attribute;
import weka.core.Instances;

/**
 *
 * @author lendle
 */
public class WekaDataFrameFactoryImpl implements DataFrameFactory{

    @Override
    public DataFrame createDataFrame(AttributeSpec[] attributes) {
        ArrayList<Attribute> attributeInfo = new ArrayList<>();
        for (AttributeSpec attributeSpec : attributes) {
            Attribute attribute=new Attribute(attributeSpec.getName(), attributeSpec.isStringType());
            attributeInfo.add(attribute);
        }
        Instances newInstances = new Instances("" + System.currentTimeMillis(), attributeInfo, 0);
        return new WekaDataFrameImpl(newInstances);
    }
    
}
