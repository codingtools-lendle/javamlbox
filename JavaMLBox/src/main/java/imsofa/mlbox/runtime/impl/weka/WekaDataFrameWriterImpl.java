/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime.impl.weka;

import imsofa.mlbox.datamodel.DataFrame;
import imsofa.mlbox.datamodel.impl.weka.WekaDataFrameImpl;
import imsofa.mlbox.runtime.DataFrameWriter;
import java.io.File;
import weka.core.converters.CSVSaver;

/**
 *
 * @author lendle
 */
public class WekaDataFrameWriterImpl implements DataFrameWriter{

    @Override
    public void save(DataFrame dataFrame, File file, String charset) throws Exception {
        CSVSaver csvSaver=new CSVSaver();
        csvSaver.setFile(file);
        csvSaver.setInstances(((WekaDataFrameImpl)dataFrame).getInstances());
        csvSaver.writeBatch();
    }
    
}
