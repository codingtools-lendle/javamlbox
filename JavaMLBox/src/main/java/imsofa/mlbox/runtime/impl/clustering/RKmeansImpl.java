/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime.impl.clustering;

import imsofa.mlbox.datamodel.DataAttribute;
import imsofa.mlbox.datamodel.DataFrame;
import imsofa.mlbox.datamodel.DataFrameRow;
import imsofa.mlbox.datamodel.DataList;
import imsofa.mlbox.datamodel.impl.DefaultDataFrameRowImpl;
import imsofa.mlbox.datamodel.impl.DefaultDataListImpl;
import imsofa.mlbox.runtime.clustering.Kmeans;
import imsofa.mlbox.runtime.clustering.KmeansParameter;
import imsofa.mlbox.runtime.clustering.KmeansResult;
import java.util.ArrayList;
import java.util.List;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import org.renjin.sexp.IntArrayVector;
import org.renjin.sexp.ListVector;

/**
 *
 * @author lendle
 */
public class RKmeansImpl implements Kmeans{

    @Override
    public KmeansResult calculate(KmeansParameter parameter) throws Exception {
        ScriptEngine scriptEngine=new ScriptEngineManager().getEngineByName("Renjin");
        DataFrame df=parameter.getDataFrame();
        List<String> names=new ArrayList<>();
        for(DataAttribute attribute : df.getAttributes()){
            DataList dataList=df.getDataList(attribute);
            if(attribute.getType().equals(String.class)){
                names.add(attribute.getName());
                scriptEngine.put(attribute.getName(), dataList.toStringArray());
            }else{
                names.add("as.character("+attribute.getName()+")");
                scriptEngine.put(attribute.getName(), dataList.toDoubleArray());
            }
        }
        scriptEngine.eval("df=data.frame("+String.join(",", names.toArray(new String[0]))+")");
        ListVector kmeans=(ListVector) scriptEngine.eval("kmeans(df, centers="+parameter.getCenters()+")");
        
        KmeansResult result=new KmeansResult();
        double [] clusters=((IntArrayVector)kmeans.get("cluster")).toDoubleArray();
        DataList clustersList=DefaultDataListImpl.fromArray(clusters);
        result.setClusters(clustersList);
        double [] centers=((org.renjin.sexp.DoubleArrayVector)kmeans.get("centers")).toDoubleArray();
        int index=0;
        List<DataFrameRow> centersList=new ArrayList<>();
        while(index<centers.length){
            DefaultDataFrameRowImpl row=new DefaultDataFrameRowImpl(df.getAttributes());
            for(DataAttribute attribute : df.getAttributes()){
                row.put(attribute.getName(), centers[index++]);
            }
            centersList.add(row);
        }
        result.setCenters(centersList.toArray(new DataFrameRow[0]));
        result.setTotalDistances(((org.renjin.primitives.summary.DeferredSum)kmeans.get("totss")).asReal());
        result.setInterClusterDistances(((org.renjin.primitives.R$primitive$$minus$deferred_dd)kmeans.get("betweenss")).asReal());
        return result;
    }
    
}
