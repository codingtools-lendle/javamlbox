/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime.impl;

import imsofa.mlbox.runtime.Core;
import imsofa.mlbox.runtime.DataFrameFactory;
import imsofa.mlbox.runtime.DataFrameLoader;
import imsofa.mlbox.runtime.DataFrameWriter;
import imsofa.mlbox.runtime.Stats;
import imsofa.mlbox.runtime.Tests;
import imsofa.mlbox.runtime.clustering.Clustering;
import imsofa.mlbox.runtime.impl.clustering.DefaultClusteringImpl;
import imsofa.mlbox.runtime.impl.tests.DefaultTestsImpl;
import imsofa.mlbox.runtime.impl.weka.WekaDataFrameFactoryImpl;
import imsofa.mlbox.runtime.impl.weka.WekaDataFrameLoaderImpl;
import imsofa.mlbox.runtime.merger.Aggregator;
import imsofa.mlbox.runtime.merger.Merger;
import imsofa.mlbox.runtime.merger.impl.weka.WekaAggregatorImpl;
import imsofa.mlbox.runtime.merger.impl.weka.WekaMergerImpl;

/**
 *
 * @author lendle
 */
public class DefaultCoreImpl implements Core{

    @Override
    public DataFrameLoader getDataFrameLoader() {
        return new WekaDataFrameLoaderImpl();
    }

    @Override
    public DataFrameWriter getDataFrameWriter() {
        return new DefaultDataFrameWriterImpl();
    }

    @Override
    public DataFrameFactory getDataFrameFactory() {
        return new WekaDataFrameFactoryImpl();
    }

    @Override
    public Stats getStats() {
        return new DefaultStatsImpl();
    }

    @Override
    public Tests getTests() {
        return new DefaultTestsImpl();
    }

    @Override
    public Merger getMerger() {
        return new WekaMergerImpl();
    }

    @Override
    public Aggregator getAggregator() {
        return new WekaAggregatorImpl();
    }

    @Override
    public Clustering getClustering() {
        return new DefaultClusteringImpl();
    }
    
}
