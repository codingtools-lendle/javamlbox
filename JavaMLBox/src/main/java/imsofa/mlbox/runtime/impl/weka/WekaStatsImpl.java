/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime.impl.weka;

import imsofa.mlbox.datamodel.DataList;
import imsofa.mlbox.datamodel.impl.weka.WekaAttributedDataListImpl;
import imsofa.mlbox.datamodel.impl.weka.WekaDataAttributeImpl;
import imsofa.mlbox.runtime.impl.DefaultStatsImpl;
import weka.core.AttributeStats;

/**
 *
 * @author lendle
 */
public class WekaStatsImpl extends DefaultStatsImpl{

    @Override
    public double mean(DataList list) {
        if(list instanceof WekaAttributedDataListImpl){
            return getWekaAttributeStats(list).numericStats.mean;
        }else{
            return super.mean(list);
        }
    }

    @Override
    public double sd(DataList list) {
        if(list instanceof WekaAttributedDataListImpl){
            return getWekaAttributeStats(list).numericStats.stdDev;
        }else{
            return super.sd(list);
        }
    }

    @Override
    public double variance(DataList list) {
        return super.variance(list);
    }

    @Override
    public double max(DataList list) {
        if(list instanceof WekaAttributedDataListImpl){
            return getWekaAttributeStats(list).numericStats.max;
        }else{
            return super.max(list);
        }
    }

    @Override
    public double min(DataList list) {
        if(list instanceof WekaAttributedDataListImpl){
            return getWekaAttributeStats(list).numericStats.min;
        }else{
            return super.min(list);
        }
    }
    
    private AttributeStats getWekaAttributeStats(DataList list){
        WekaAttributedDataListImpl wekaAttributedDataListImpl=(WekaAttributedDataListImpl) list;
        return ((WekaDataAttributeImpl)wekaAttributedDataListImpl.getAttribute()).getStats();
    }
    
}
