/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime.impl.tests;

import imsofa.mlbox.datamodel.DataList;
import imsofa.mlbox.runtime.Tests;
import imsofa.mlbox.runtime.tests.TTestResult;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import org.apache.commons.math3.stat.correlation.KendallsCorrelation;
import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import org.apache.commons.math3.stat.correlation.SpearmansCorrelation;
import org.apache.commons.math3.stat.inference.TTest;
import org.renjin.sexp.ListVector;

/**
 *
 * @author lendle
 */
public class DefaultTestsImpl implements Tests{

    @Override
    public double pearsonCorr(DataList list1, DataList list2) {
        return new PearsonsCorrelation().correlation(list1.toDoubleArray(), list2.toDoubleArray());
    }

    @Override
    public double kendallsCorr(DataList list1, DataList list2) {
        return new KendallsCorrelation().correlation(list1.toDoubleArray(), list2.toDoubleArray());
    }

    @Override
    public double spearmanCorr(DataList list1, DataList list2) {
        return new SpearmansCorrelation().correlation(list1.toDoubleArray(), list2.toDoubleArray());
    }

    @Override
    public TTestResult tTest(DataList list1, DataList list2) throws Exception {
        ScriptEngine scriptEngine=new ScriptEngineManager().getEngineByName("Renjin");
        scriptEngine.put("list1", list1.toDoubleArray());
        scriptEngine.put("list2", list2.toDoubleArray());
        ListVector ret=(ListVector) scriptEngine.eval("t.test(list1, list2);");
        return this.convertRTTestResult(ret);
    }

    @Override
    public TTestResult pairedTTest(DataList list1, DataList list2)  throws Exception{
        ScriptEngine scriptEngine=new ScriptEngineManager().getEngineByName("Renjin");
        scriptEngine.put("list1", list1.toDoubleArray());
        scriptEngine.put("list2", list2.toDoubleArray());
        ListVector ret=(ListVector) scriptEngine.eval("t.test(list1, list2, paired=TRUE);");
        return this.convertRTTestResult(ret);
    }
    
    private TTestResult convertRTTestResult(ListVector rResult){
        TTestResult tTestResult=new TTestResult();
        tTestResult.setT(rResult.get(0).asReal());
        tTestResult.setDf(rResult.get(1).asReal());
        tTestResult.setP(rResult.get(2).asReal());
        org.renjin.sexp.DoubleArrayVector doubleArrayVector=(org.renjin.sexp.DoubleArrayVector) rResult.get(3);
        tTestResult.setConfidenceInterval(new double[]{doubleArrayVector.get(0), doubleArrayVector.get(1)});
        return tTestResult;
    }
}
