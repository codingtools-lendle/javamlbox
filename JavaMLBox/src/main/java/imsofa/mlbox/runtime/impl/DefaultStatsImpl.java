/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime.impl;

import imsofa.mlbox.datamodel.DataList;
import imsofa.mlbox.runtime.Stats;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

/**
 *
 * @author lendle
 */
public class DefaultStatsImpl implements Stats{

    @Override
    public double mean(DataList list) {
        DescriptiveStatistics stats=new DescriptiveStatistics(list.toDoubleArray());
        return stats.getMean();
    }

    @Override
    public double median(DataList list) {
        DescriptiveStatistics stats=new DescriptiveStatistics(list.toDoubleArray());
        return stats.getPercentile(50);
    }

    @Override
    public double sd(DataList list) {
        DescriptiveStatistics stats=new DescriptiveStatistics(list.toDoubleArray());
        return stats.getStandardDeviation();
    }

    @Override
    public double variance(DataList list) {
        DescriptiveStatistics stats=new DescriptiveStatistics(list.toDoubleArray());
        return stats.getVariance();
    }

    @Override
    public double max(DataList list) {
        DescriptiveStatistics stats=new DescriptiveStatistics(list.toDoubleArray());
        return stats.getMax();
    }

    @Override
    public double min(DataList list) {
        DescriptiveStatistics stats=new DescriptiveStatistics(list.toDoubleArray());
        return stats.getMin();
    }

    @Override
    public double[] quantile(DataList list, double[] positions) {
        DescriptiveStatistics stats=new DescriptiveStatistics(list.toDoubleArray());
        double [] ret=new double[positions.length];
        for(int i=0; i<positions.length; i++){
            ret[i]=(positions[i]==0)?stats.getMin():stats.getPercentile(positions[i]);
        }
        return ret;
    }

    @Override
    public double[] quantile(DataList list) {
        return this.quantile(list, new double[]{0, 25, 50, 75, 100});
    }

    @Override
    public double sum(DataList list) {
        DescriptiveStatistics stats=new DescriptiveStatistics(list.toDoubleArray());
        return stats.getSum();
    }
    
}
