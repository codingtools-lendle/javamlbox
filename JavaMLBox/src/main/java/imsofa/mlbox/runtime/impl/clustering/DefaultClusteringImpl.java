/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime.impl.clustering;

import imsofa.mlbox.runtime.clustering.Clustering;
import imsofa.mlbox.runtime.clustering.Kmeans;

/**
 *
 * @author lendle
 */
public class DefaultClusteringImpl implements Clustering{

    @Override
    public Kmeans getKmeans() {
        return new RKmeansImpl();
    }
    
}
