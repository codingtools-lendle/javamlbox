/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime.impl.weka;

import imsofa.mlbox.datamodel.DataFrame;
import imsofa.mlbox.datamodel.impl.weka.WekaDataFrameImpl;
import imsofa.mlbox.runtime.DataFrameLoader;
import java.io.File;
import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.core.converters.ConverterUtils;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.NominalToString;

/**
 *
 * @author lendle
 */
public class WekaDataFrameLoaderImpl implements DataFrameLoader {

    @Override
    public DataFrame load(File file, String charset) throws Exception {
        String lowerCaseFileName = file.getName().toLowerCase();
        if (lowerCaseFileName.endsWith(".csv")) {
            return this.loadCSV(file);
        } else if (lowerCaseFileName.endsWith(".arff")) {
            return this.loadArff(file);
        } else {
            throw new Exception("unsupported file extension");
        }
    }

    private DataFrame loadCSV(File file) throws Exception {
        CSVLoader loader = new CSVLoader();
        loader.setSource(file);
        Instances data = loader.getDataSet();
        NominalToString filter = new NominalToString();
        filter.setOptions(new String[]{"-C", "first-last"});
        filter.setInputFormat(data);
        data=Filter.useFilter(data, filter);
        return new WekaDataFrameImpl(data);
    }

    private DataFrame loadArff(File file) throws Exception {
        ConverterUtils.DataSource source = new ConverterUtils.DataSource(file.getAbsolutePath());
        Instances data = source.getDataSet();
        return new WekaDataFrameImpl(data);
    }
}
