/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime.impl;

import imsofa.mlbox.datamodel.DataAttribute;
import imsofa.mlbox.datamodel.DataFrame;
import imsofa.mlbox.datamodel.DataFrameRow;
import imsofa.mlbox.runtime.DataFrameWriter;
import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

/**
 *
 * @author lendle
 */
public class DefaultDataFrameWriterImpl implements DataFrameWriter{

    @Override
    public void save(DataFrame dataFrame, File file, String charset) throws Exception {
        List<String> attributeNames=new ArrayList<>();
        for(DataAttribute attribute : dataFrame.getAttributes()){
            attributeNames.add(attribute.getName());
        }
        CSVPrinter cSVPrinter=CSVFormat.DEFAULT.withHeader(attributeNames.toArray(new String[0])).print(file, Charset.forName(charset));
        for(int i=0; i<dataFrame.size(); i++){
            DataFrameRow row=dataFrame.getRow(i);
            cSVPrinter.printRecord(row.getValues());
        }
        cSVPrinter.close();
    }
    
}
