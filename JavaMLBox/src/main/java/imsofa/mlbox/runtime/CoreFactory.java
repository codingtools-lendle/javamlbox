/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime;

import imsofa.mlbox.runtime.impl.DefaultCoreImpl;

/**
 *
 * @author lendle
 */
public class CoreFactory {
    public static Core getCore(){
        return new DefaultCoreImpl();
    }
}
