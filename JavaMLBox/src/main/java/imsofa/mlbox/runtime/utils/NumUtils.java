/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.runtime.utils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lendle
 */
public class NumUtils {
    public static int [] generateIntegerSequence(int start, int end, int step){
        List<Integer> values=new ArrayList<>();
        for(int i=start; i<=end; i=i+step){
            values.add(i);
        }
        int[] array = values.stream().mapToInt(i->i).toArray();
        return array;
    }
}
