/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.datamodel;

import java.util.List;

/**
 *
 * @author lendle
 */
public interface DataFrameRow {
    public List<DataAttribute> getAttributes();
    public Object getValue(DataAttribute attribute);
    public Object getValue(String attributeName);
    public Double getDoubleValue(DataAttribute attribute);
    public Double getDoubleValue(String attributeName);
    public String getStringValue(DataAttribute attribute);
    public String getStringValue(String attributeName);
    public Object [] getValues();
}
