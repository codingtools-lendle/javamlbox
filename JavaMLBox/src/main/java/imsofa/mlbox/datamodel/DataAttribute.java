/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.datamodel;

/**
 *
 * @author lendle
 */
public interface DataAttribute{
    public String getName();
    public Class getType();
}
