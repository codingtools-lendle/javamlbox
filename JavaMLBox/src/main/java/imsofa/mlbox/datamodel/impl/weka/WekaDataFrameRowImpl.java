/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.datamodel.impl.weka;

import imsofa.mlbox.datamodel.DataAttribute;
import imsofa.mlbox.datamodel.DataFrameRow;
import java.util.ArrayList;
import java.util.List;
import weka.core.Attribute;
import weka.core.Instance;

/**
 *
 * @author lendle
 */
public class WekaDataFrameRowImpl implements DataFrameRow{
    private Instance instance=null;
    private List<DataAttribute> attributes=null;

    public WekaDataFrameRowImpl(Instance instance, List<DataAttribute> attributes) {
        this.instance=instance;
        this.attributes=attributes;
    }
    
    @Override
    public List<DataAttribute> getAttributes() {
        return new ArrayList<DataAttribute>(attributes);
    }

    @Override
    public Object getValue(DataAttribute attribute) {
        WekaDataAttributeImpl wekaAttribute=(WekaDataAttributeImpl) attribute;
        return this.getValue(wekaAttribute.getAttribute());
    }
    
    private Object getValue(Attribute attribute){
        if(attribute.isNumeric()){
            return instance.value(attribute);
        }else{
            return instance.stringValue(attribute);
        }
    }

    @Override
    public Object getValue(String attributeName) {
        return getValue(instance.dataset().attribute(attributeName));
    }

    @Override
    public Double getDoubleValue(DataAttribute attribute) {
        return (Double) this.getValue(attribute);
    }

    @Override
    public Double getDoubleValue(String attributeName) {
        return (Double) this.getValue(attributeName);
    }

    @Override
    public String getStringValue(DataAttribute attribute) {
        return (String) this.getValue(attribute);
    }

    @Override
    public String getStringValue(String attributeName) {
        return (String) this.getValue(attributeName);
    }

    @Override
    public Object[] getValues() {
        Object [] ret=new Object[this.attributes.size()];
        int index=0;
        for(DataAttribute attribute : this.getAttributes()){
            ret[index++]=this.getValue(attribute);
        }
        return ret;
    }
    
}
