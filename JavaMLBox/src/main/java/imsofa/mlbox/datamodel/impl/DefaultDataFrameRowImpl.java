/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.datamodel.impl;

import imsofa.mlbox.datamodel.DataAttribute;
import imsofa.mlbox.datamodel.DataFrameRow;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author lendle
 */
public class DefaultDataFrameRowImpl extends HashMap<String, Object> implements DataFrameRow{
    private List<DataAttribute> attributes=null;

    public DefaultDataFrameRowImpl(List<DataAttribute> attributes) {
        this.attributes=attributes;
    }
    
    @Override
    public List<DataAttribute> getAttributes() {
        return new ArrayList<DataAttribute>(attributes);
    }

    @Override
    public Object getValue(DataAttribute attribute) {
        return this.get(attribute.getName());
    }

    @Override
    public Object getValue(String attributeName) {
        return this.get(attributeName);
    }

    @Override
    public Double getDoubleValue(DataAttribute attribute) {
        return (Double) this.getValue(attribute);
    }

    @Override
    public Double getDoubleValue(String attributeName) {
        return (Double) this.getValue(attributeName);
    }

    @Override
    public String getStringValue(DataAttribute attribute) {
        return (String) this.getValue(attribute);
    }

    @Override
    public String getStringValue(String attributeName) {
        return (String) this.getValue(attributeName);
    }

    @Override
    public Object[] getValues() {
        Object [] ret=new Object[this.attributes.size()];
        int index=0;
        for(DataAttribute attribute : this.getAttributes()){
            ret[index++]=this.getValue(attribute);
        }
        return ret;
    }
}
