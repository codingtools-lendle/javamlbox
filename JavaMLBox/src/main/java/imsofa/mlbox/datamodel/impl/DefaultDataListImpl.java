/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.datamodel.impl;

import imsofa.mlbox.datamodel.DataList;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotEmpty;

/**
 *
 * @author lendle
 */
public class DefaultDataListImpl<T> extends ArrayList<T> implements DataList<T>{
    private Class type=null;

    public DefaultDataListImpl(Class type) {
        this.type=type;
    }
    
    
    
    public Class getType() {
        return type;
    }

    public void setType(Class type) {
        this.type = type;
    }

    @Override
    public DataList<T> getSublist(int from, int to) {
        return fromList(type, this.subList(from, to));
    }
    
    public static DataList fromList(Class type, @NotEmpty List list){
        DefaultDataListImpl dataList=new DefaultDataListImpl(list.get(0).getClass().equals(String.class)?String.class:Double.class);
        dataList.addAll(list);
        return dataList;
    }
    
    public static DataList fromArray(double [] array){
        DefaultDataListImpl dataList=new DefaultDataListImpl(Double.class);
        for(Object o : array){
            dataList.add(o);
        }
        return dataList;
    }
    
    public static DataList fromArray(String [] array){
        DefaultDataListImpl dataList=new DefaultDataListImpl(String.class);
        for(Object o : array){
            dataList.add(o);
        }
        return dataList;
    }

    @Override
    public T[] toArray() {
        T[] array=(T[]) Array.newInstance(type, size());
        int i=0;
        for(T value : this){
            array[i++]=value;
        }
        return array;
    }

    @Override
    public double[] toDoubleArray() {
        double[] array=new double[size()];
        int i=0;
        for(T value : this){
            array[i++]=Double.valueOf(""+value);
        }
        return array;
    }

    @Override
    public String[] toStringArray() {
        String [] array=new String[size()];
        int i=0;
        for(T value : this){
            array[i++]=(String) value;
        }
        return array;
    }
    
}
