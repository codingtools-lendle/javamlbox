/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.datamodel.impl.weka;

import imsofa.mlbox.datamodel.impl.DefaultDataAttributeImpl;
import weka.core.Attribute;
import weka.core.AttributeStats;

/**
 *
 * @author lendle
 */
public class WekaDataAttributeImpl extends DefaultDataAttributeImpl{
    private Attribute attribute=null;
    private AttributeStats stats=null;

    public WekaDataAttributeImpl(Attribute attribute, AttributeStats stats) {
        this.attribute=attribute;
        this.stats=stats;
    }
    
    
    public Attribute getAttribute() {
        return attribute;
    }

    public void setAttribute(Attribute attribute) {
        this.attribute = attribute;
    }

    public AttributeStats getStats() {
        return stats;
    }

    public void setStats(AttributeStats stats) {
        this.stats = stats;
    }

    @Override
    public Class getType() {
        if(attribute.isNominal() || attribute.isString()){
            return String.class;
        }else{
            return Double.class;
        }
    }
    
    
}
