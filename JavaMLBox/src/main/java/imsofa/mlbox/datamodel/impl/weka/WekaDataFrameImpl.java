/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.datamodel.impl.weka;

import imsofa.mlbox.datamodel.AttributedDataList;
import imsofa.mlbox.datamodel.DataFrame;
import java.util.List;
import weka.core.Instances;
import imsofa.mlbox.datamodel.DataAttribute;
import imsofa.mlbox.datamodel.DataFrameFilter;
import imsofa.mlbox.datamodel.DataFrameRow;
import imsofa.mlbox.datamodel.DataList;
import imsofa.mlbox.runtime.DataFrameFactory.AttributeSpec;
import imsofa.mlbox.runtime.impl.weka.WekaDataFrameFactoryImpl;
import imsofa.mlbox.runtime.utils.NumUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

/**
 *
 * @author lendle
 */
public class WekaDataFrameImpl implements DataFrame {

    private Instances instances = null;
//    private Map<String, WekaDataAttributeImpl> attributesMap = new HashMap<>();
//    private List<WekaDataAttributeImpl> attributeList = new ArrayList<>();

    public Instances getInstances() {
        return instances;
    }

    public WekaDataFrameImpl(Instances instances) {
        this.instances = instances;
//        Enumeration<Attribute> en = instances.enumerateAttributes();
//        int index = 0;
//        while (en.hasMoreElements()) {
//            Attribute attribute = en.nextElement();
//            WekaDataAttributeImpl dataAttribute = new WekaDataAttributeImpl(attribute, instances.attributeStats(index));
//            dataAttribute.setName(attribute.name());
//            if (attribute.isNumeric()) {
//                dataAttribute.setType(Double.class);
//            } else if (attribute.isDate()) {
//                dataAttribute.setType(Date.class);
//            } else {
//                dataAttribute.setType(String.class);
//            }
//            attributesMap.put(attribute.name(), dataAttribute);
//            attributeList.add(dataAttribute);
//            index++;
//        }
    }

    @Override
    public List<DataAttribute> getAttributes() {
        List<DataAttribute> attributeList = new ArrayList<>();
        Enumeration<Attribute> en = instances.enumerateAttributes();
        int index = 0;
        while (en.hasMoreElements()) {
            Attribute attribute = en.nextElement();
            WekaDataAttributeImpl dataAttribute = new WekaDataAttributeImpl(attribute, instances.attributeStats(index));
            dataAttribute.setName(attribute.name());
            if (attribute.isNumeric()) {
                dataAttribute.setType(Double.class);
            } else if (attribute.isDate()) {
                dataAttribute.setType(Date.class);
            } else {
                dataAttribute.setType(String.class);
            }
            attributeList.add(dataAttribute);
            index++;
        }
        return attributeList;
    }

    @Override
    public AttributedDataList getDataList(DataAttribute attribute) {
        WekaAttributedDataListImpl list = new WekaAttributedDataListImpl((WekaDataAttributeImpl) attribute);
        Enumeration<Instance> en = instances.enumerateInstances();
        while (en.hasMoreElements()) {
            Instance instance = en.nextElement();
            if(attribute.getType().equals(String.class)){
                list.add(instance.stringValue(((WekaDataAttributeImpl) attribute).getAttribute()));
            }else{
                list.add(instance.value(((WekaDataAttributeImpl) attribute).getAttribute()));
            }
        }
        return list;
    }

    @Override
    public AttributedDataList getDataList(String attributeName) {
        return getDataList(getAttribute(attributeName));
    }

    @Override
    public DataAttribute getAttribute(String attributeName) {
        Attribute attribute=instances.attribute(attributeName);
        WekaDataAttributeImpl dataAttribute = new WekaDataAttributeImpl(attribute, instances.attributeStats(attribute.index()));
        return dataAttribute;
    }

    @Override
    public DataFrame filter(DataFrameFilter filter) {
        Instances copy = new Instances(instances, 0);
        List<DataAttribute> attributes = this.getAttributes();
        for (int i = 0; i < instances.numInstances(); i++) {
            Instance instance = (Instance) instances.get(i).copy();
            if (filter.accept(this, new WekaDataFrameRowImpl(instance, attributes), i)) {
                copy.add(instance);
            }
        }
        return new WekaDataFrameImpl(copy);
    }

    @Override
    public DataFrame filter(int[] rows) {
        Instances copy = new Instances(instances, 0);
        Map<Integer, String> indexMap = new HashMap<>();//for quick row number filtering
        for (int i : rows) {
            indexMap.put(i, "");
        }
        for (int i = 0; i < instances.numInstances(); i++) {
            if (indexMap.containsKey(i)) {
                Instance instance = (Instance) instances.get(i).copy();
                copy.add(instance);
            }
        }
        return new WekaDataFrameImpl(copy);
    }

    @Override
    public DataFrame filter(int from, int to) {
        return this.filter(NumUtils.generateIntegerSequence(from, to, 1));
    }

    @Override
    public DataFrame project(String[] attributeNames) {
        int[] attributeIndices = new int[attributeNames.length];
        Map<String, String> indexMap = new HashMap<>();//for quick attributeName filtering
        int index = 0;
        for (String name : attributeNames) {
            indexMap.put(name, "");
            attributeIndices[index++] = ((WekaDataAttributeImpl) this.getAttribute(name)).getAttribute().index();
        }

        Remove removeFilter = new Remove();

        removeFilter.setAttributeIndicesArray(attributeIndices);
        removeFilter.setInvertSelection(true);
        try {
            removeFilter.setInputFormat(instances);
            Instances newData = Filter.useFilter(instances, removeFilter);
            return new WekaDataFrameImpl(newData);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public DataFrameRow getRow(int index) {
        Instance instance = this.instances.instance(index);
        WekaDataFrameRowImpl row = new WekaDataFrameRowImpl(instance, this.getAttributes());
        return row;
    }

    @Override
    public void addRow(Object[] attributes) {
        DenseInstance instance = new DenseInstance(this.getAttributes().size());
        int index = 0;
        for (DataAttribute attribute : this.getAttributes()) {
            if (Number.class.isAssignableFrom(attribute.getType())) {
                instance.setValue(((WekaDataAttributeImpl)attribute).getAttribute(), Double.valueOf("" + attributes[index]));
            } else {
                instance.setValue(((WekaDataAttributeImpl)attribute).getAttribute(), (String) attributes[index]);
            }
            index++;
        }
        instances.add(instance);
    }

    @Override
    public void addRow(DataFrameRow row) {
        this.addRow(row.getValues());
    }

    @Override
    public void addRow(DataFrame df) {
        for (int i = 0; i < df.size(); i++) {
            this.addRow(df.getRow(i));
        }
    }

    @Override
    public void addColumn(AttributedDataList dataList) {
        this.addColumn(dataList.getAttribute().getName(), dataList.getType(), dataList.toArray());
    }

    @Override
    public void addColumn(String attributeName, Class type, DataList dataList) {
        this.addColumn(attributeName, type, dataList.toArray());
    }

    @Override
    public void addColumn(String attributeName, Class type, Object[] values) {
        Attribute attribute = new Attribute(attributeName, type.equals(String.class));
        instances.insertAttributeAt(attribute, this.getAttributes().size());
        Enumeration<Instance> enumInstances = instances.enumerateInstances();
        int index = 0;
        attribute=instances.attribute(attributeName);
        while (enumInstances.hasMoreElements()) {
            Instance instance = enumInstances.nextElement();
            if (Number.class.isAssignableFrom(type)) {
                Double doubleValue = (Double) values[index++];
                if (doubleValue.equals(Double.NaN)) {
                    instance.setMissing(attribute);
                } else {
                    instance.setValue(attribute, (Double) doubleValue);
                }
            } else {
                String stringValue = (String) values[index++];
                if (stringValue==null) {
                    instance.setMissing(attribute);
                } else {
                    instance.setValue(attribute, stringValue);
                }
            }
        }
    }

    @Override
    public long size() {
        return this.instances.size();
    }

    @Override
    public DataFrame sort(final SortSpec[] sortSpecs) {
        return this.sort(new Comparator<DataFrameRow>() {
            @Override
            public int compare(DataFrameRow o1, DataFrameRow o2) {
                for(SortSpec sortSpec : sortSpecs){
                    Object value1=o1.getValue(sortSpec.getAttributeName());
                    Object value2=o2.getValue(sortSpec.getAttributeName());
                    if(value1==null){
                        if(value2==null){
                            continue;
                        }else{
                            return sortSpec.isAscending()?-1:1;
                        }
                    }else{
                        if(value1.equals(value2)){
                            continue;
                        }
                        if(sortSpec.isAscending()){
                            return ((Comparable)value1).compareTo(value2);
                        }else{
                            return ((Comparable)value2).compareTo(value1);
                        }
                    }
                }
                return 0;
            }
        });
    }

    @Override
    public DataFrame sort(Comparator<DataFrameRow> comparator) {
        List<DataFrameRow> rows=new ArrayList<>();
        for(int i=0; i<this.size(); i++){
            rows.add(this.getRow(i));
        }
        Collections.sort(rows, comparator);
        WekaDataFrameFactoryImpl factory=new WekaDataFrameFactoryImpl();
        List<DataAttribute> attributes=this.getAttributes();
        AttributeSpec [] attributeSpecs=new AttributeSpec[attributes.size()];
        for(int i=0; i<attributes.size(); i++){
            attributeSpecs[i]=new AttributeSpec(attributes.get(i).getName(), attributes.get(i).getType().equals(String.class));
        }
        DataFrame newDataFrame=factory.createDataFrame(attributeSpecs);
        for(DataFrameRow row : rows){
            newDataFrame.addRow(row);
        }
        return newDataFrame;
    }

    @Override
    public DataFrame sort(String[] attributeNames) {
        SortSpec [] sortSpecs=new SortSpec[attributeNames.length];
        for(int i=0; i<attributeNames.length; i++){
            sortSpecs[i]=new SortSpec(attributeNames[i], false);
        }
        return this.sort(sortSpecs);
    }

    @Override
    public String toString() {
        return instances.toString();
    }
}
