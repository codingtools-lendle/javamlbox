/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.datamodel.impl.weka;

import imsofa.mlbox.datamodel.AttributedDataList;
import imsofa.mlbox.datamodel.DataAttribute;
import imsofa.mlbox.datamodel.impl.DefaultDataListImpl;

/**
 *
 * @author lendle
 */
public class WekaAttributedDataListImpl extends DefaultDataListImpl implements AttributedDataList{
    private WekaDataAttributeImpl attribute=null;

    public WekaAttributedDataListImpl(WekaDataAttributeImpl attribute) {
        super(attribute.getType());
        this.attribute=attribute;
    }

    @Override
    public Class getType() {
        return attribute.getType();
    }
    
    @Override
    public DataAttribute getAttribute() {
        return this.attribute;
    }
    
}
