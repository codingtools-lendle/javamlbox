/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.datamodel;

import java.util.Comparator;
import java.util.List;

/**
 *
 * @author lendle
 */
public interface DataFrame {
    public List<DataAttribute> getAttributes();
    public DataAttribute getAttribute(String attributeName);
    public AttributedDataList getDataList(DataAttribute attribute);
    public AttributedDataList getDataList(String attributeName);
    public DataFrame filter(DataFrameFilter filter);
    public DataFrame filter(int [] rows);
    /**
     * 
     * @param from inclusive
     * @param to inclusive
     * @return 
     */
    public DataFrame filter(int from, int to);
    public DataFrame project(String [] attributeNames);
    public DataFrameRow getRow(int index);
    public void addRow(Object [] attributes);
    public void addRow(DataFrameRow row);
    public void addRow(DataFrame df);
    public void addColumn(AttributedDataList dataList);
    public void addColumn(String attributeName, Class type, DataList dataList);
    public void addColumn(String attributeName, Class type, Object [] values);
    
    public long size();
    public DataFrame sort(SortSpec [] sortSpecs);
    public DataFrame sort(Comparator<DataFrameRow> comparator);
    /**
     * sort in descending order
     * @param attributeNames
     * @return 
     */
    public DataFrame sort(String [] attributeNames);
    
    public static class SortSpec{
        private String attributeName=null;
        private boolean ascending=true;

        public SortSpec(String attributeName, boolean ascending) {
            this.attributeName=attributeName;
            this.ascending=ascending;
        }

        public SortSpec(String attributeName) {
            this(attributeName, true);
        }

        public String getAttributeName() {
            return attributeName;
        }

        public void setAttributeName(String attributeName) {
            this.attributeName = attributeName;
        }

        public boolean isAscending() {
            return ascending;
        }

        public void setAscending(boolean ascending) {
            this.ascending = ascending;
        }
        
        
    }
}
