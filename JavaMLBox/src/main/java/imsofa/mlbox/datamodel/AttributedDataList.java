/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.datamodel;

/**
 * a DataList that is governed by an attribute
 * @author lendle
 */
public interface AttributedDataList<T> extends DataList<T>{
    public DataAttribute getAttribute();
}
