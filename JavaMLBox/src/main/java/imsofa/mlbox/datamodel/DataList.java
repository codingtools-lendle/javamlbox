/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox.datamodel;

/**
 *
 * @author lendle
 */
public interface DataList<T> {
    public Class getType();
    public int size();
    public T get(int index);
    public DataList<T> getSublist(int from, int to);
    public T[] toArray();
    public double [] toDoubleArray();
    public String [] toStringArray();
}
