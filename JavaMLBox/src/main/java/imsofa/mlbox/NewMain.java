/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.mlbox;

import imsofa.mlbox.datamodel.AttributedDataList;
import imsofa.mlbox.datamodel.DataFrame;
import imsofa.mlbox.datamodel.impl.weka.WekaDataFrameImpl;
import imsofa.mlbox.runtime.DataFrameLoader;
import imsofa.mlbox.runtime.Stats;
import imsofa.mlbox.runtime.Tests;
import imsofa.mlbox.runtime.impl.DefaultDataFrameWriterImpl;
import imsofa.mlbox.runtime.impl.tests.DefaultTestsImpl;
import imsofa.mlbox.runtime.impl.weka.WekaDataFrameLoaderImpl;
import imsofa.mlbox.runtime.impl.weka.WekaStatsImpl;
import imsofa.mlbox.runtime.merger.AggregatingAttribute;
import imsofa.mlbox.runtime.merger.AggregatingMethod;
import imsofa.mlbox.runtime.merger.impl.weka.WekaAggregatorImpl;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import org.renjin.sexp.ListVector;
import weka.core.Attribute;
import weka.core.Instances;

/**
 *
 * @author lendle
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception{
        // TODO code application logic here
        DataFrameLoader loader=new WekaDataFrameLoaderImpl();
        DataFrame df=loader.load(new File("iris.csv"), "utf-8");
        Stats stats=new WekaStatsImpl();
        Tests tests=new DefaultTestsImpl();
        System.out.println(stats.mean(df.getDataList("sepallength")));
        System.out.println(stats.sd(df.getDataList("sepallength")));
        System.out.println(stats.min(df.getDataList("sepallength")));
        System.out.println(stats.max(df.getDataList("sepallength")));
        System.out.println(Arrays.toString(stats.quantile(df.getDataList("sepallength"))));
        System.out.println(tests.kendallsCorr(df.getDataList("sepallength"), df.getDataList("petallength")));
        System.out.println(tests.pearsonCorr(df.getDataList("sepallength"), df.getDataList("petallength")));
        System.out.println(tests.spearmanCorr(df.getDataList("sepallength"), df.getDataList("petallength")));
        System.out.println(tests.tTest(df.getDataList("sepallength"), df.getDataList("petallength")));
        System.out.println(tests.pairedTTest(df.getDataList("sepallength"), df.getDataList("petallength")));
        DataFrame df1=df.filter(new int[]{0,1,2});
        System.out.println(df1.getDataList("sepallength").size());
        DataFrame df2=df.project(new String[]{"petallength", "petalwidth"});
        System.out.println(df2.getAttributes().get(0).getName()+":"+df.getAttributes().get(0).getName());
        
        Instances instances=new Instances(""+System.currentTimeMillis(), new ArrayList<Attribute>(
                Arrays.asList(
                        new Attribute("t1"),new Attribute("t2"),new Attribute("t3")
                )), 0);
        WekaDataFrameImpl df3=new WekaDataFrameImpl(instances);
        WekaDataFrameImpl df4=(WekaDataFrameImpl) df.filter(0, 4);
        df4.addColumn("t1", String.class, new Object[]{"A", "B", "C", "D", "E"});
        df4.addRow(new Object[]{1,2,3,4,"Setosa", "F"});
        System.out.println(((WekaDataFrameImpl)df4).getInstances());
        WekaAggregatorImpl aggregator=new WekaAggregatorImpl();
        WekaDataFrameImpl df5=(WekaDataFrameImpl) aggregator.aggregate(df, new String[]{"variety"}, new AggregatingAttribute[]{new AggregatingAttribute("sepallength", AggregatingMethod.mean),
            new AggregatingAttribute("sepalwidth", AggregatingMethod.mean)});
        System.out.println(df5.getInstances());
        WekaDataFrameImpl df6=(WekaDataFrameImpl) df.sort(new DataFrame.SortSpec[]{
            new DataFrame.SortSpec("variety"),
            new DataFrame.SortSpec("sepallength", false)
        });
        System.out.println(df6.getInstances());
        ScriptEngine scriptEngine=new ScriptEngineManager().getEngineByName("Renjin");
        ListVector ret=(ListVector) scriptEngine.eval("t.test(runif(100), runif(100))");
        System.out.println(ret.get(3).getClass());
        DefaultDataFrameWriterImpl dataFrameWriter=new DefaultDataFrameWriterImpl();
        dataFrameWriter.save(df4, new File("testSave.csv"), "utf-8");
        DataFrame df7=loader.load(new File("testSave.csv"), "utf-8");
        System.out.println("df7...............");
        System.out.println(((WekaDataFrameImpl)df7).getInstances());
        System.out.println(Arrays.deepToString(df7.getDataList("t1").toArray()));
    }
    
}
